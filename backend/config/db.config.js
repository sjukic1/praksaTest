module.exports = {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "root",
    DB: "postgres",
    dialect: "postgres",
    pool: {
        max: 10,
        min: 0,
        idle: 10000,
    }
}